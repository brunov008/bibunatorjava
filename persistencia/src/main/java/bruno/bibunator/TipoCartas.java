package bruno.bibunator;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TipoCartas", catalog = "bibunatorUni")
public class TipoCartas implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "idTipoCarta")
	private Integer id;
	
	@Column(name = "nomeTipo")
	private String nomeTipo;
	
	@Column(name = "usuario_idusuario")
	//@ManyToOne(targetEntity = Usuario.class) TODO
	private String usuario_idusuario;
	
	public TipoCartas() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeTipo() {
		return nomeTipo;
	}

	public void setNomeTipo(String nomeTipo) {
		this.nomeTipo = nomeTipo;
	}

	public String getUsuario_idusuario() {
		return usuario_idusuario;
	}

	public void setUsuario_idusuario(String usuario_idusuario) {
		this.usuario_idusuario = usuario_idusuario;
	}
	
	

}
