FROM jboss/wildfly:18.0.1.Final

#Permissoes de root para instalar git 
USER root

#Instalar git
RUN yum install git -y

#Remover arquivo standalone da imagem
RUN rm -f /opt/jboss/wildfly/standalone/configuration/standalone.xml

#Copiar o modulo do mysql para a pasta modulo do jboss
COPY infra/wildfly/mysql /opt/jboss/wildfly/modules/system/layers/base/com/mysql

#Copiar Standalone customizado para a imagem
COPY infra/wildfly/standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml

#Maven
COPY infra/apache-maven-3.6.3 apache

#Git clone do repositorio
RUN git clone https://brunov008@bitbucket.org/brunov008/bibunatorjava.git

#Rodar comando mvn install
RUN ./apache/bin/mvn install -f bibunatorjava/pom.xml

#Mover EAR para a pasta deployment do wildfly
RUN cp -R ~/.m2/repository/bruno-bibunator/ear/1.0-SNAPSHOT/ear-1.0-SNAPSHOT.ear /opt/jboss/wildfly/standalone/deployments/

#Permissoes para o script wait-for-it
CMD ["chmod", "777", "bibunatorjava/wait-for-it.sh"]

#Iniciar servidor wildfly em background
CMD ["./bibunatorjava/wait-for-it.sh","db:3306","--","/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]


