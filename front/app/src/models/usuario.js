export class Usuario{

	set nomeUsuario(value){
		this.nome = value
	}

	set senhaUsuario(value){
		this.senha = value
	}

	set idusuario(value){
		this.idusuario = value
	}

	set emailUsuario(value){
		this.email = value
	}

	get idusuario(){
		return this.idusuario
	}

	get nomeUsuario(){
		return this.nome
	}

	get emailUsuario(){
		return this.email
	}

	get senhaUsuario(){
		return this.senha
	}
}