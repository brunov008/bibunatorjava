export class Utility{
	static validateEmail = value => {
		const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		return value.match(emailRex)
	}

	static validateName = value => {
		if(value != null && value.length >= 3){
			return true
		}else{
			return false
		}
	}

	static validatePassword = value => {
		if(value != null && value.length >= 3){
			return true
		}else{
			return false
		}
	}
}