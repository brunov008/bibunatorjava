import React, { Component } from 'react'
import {Modal,Button,Form,Container} from 'react-bootstrap';

export class CustomModal extends Component{
	constructor(props){
		super(props)
		this.state = {
			tipo:null
		}
	}

	handleSubmit = async event =>{
		event.preventDefault()
		if(this.state.tipo != null) this.props.onFormComplete(this.state.tipo)
	}

	handleChange = async event =>{
		const { target } = event
		const value = target.value

		if(value != ''){
			this.setState({tipo:value})
		}
	}

	render(){
		return(
			<Modal
		      {...this.props}
		      size="lg"
		      aria-labelledby="contained-modal-title-vcenter"
		      centered>
		      <Modal.Header closeButton>
		        <Modal.Title id="contained-modal-title-vcenter">
		          Adicionar
		        </Modal.Title>
		      </Modal.Header>
		      <Modal.Body>
		        <Container fluid="true">
		        	<Container className="justify-content-center">
		        		<Form
		        			noValidate
		        			onSubmit={this.handleSubmit}>
		        			<Form.Group>
			        			<Form.Control
			        				id="tipo"
			        				type="text"
			        				placeholder="Tipo"
			        				onChange={this.handleChange}
			        				required/> 
		        			</Form.Group>

		        			<Button variant="primary" type="submit">
						    	Inserir
						 	</Button>
		        		</Form>
		        	</Container>
		        </Container>
		      </Modal.Body>
		    </Modal>
		)
	}
}