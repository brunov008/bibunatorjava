import React, { Component } from 'react'
import {Container,Card,Button,CardDeck} from 'react-bootstrap'

import {DrunknatorService} from '../../services'

export class Listar extends Component{

	constructor(props){
		super(props)
		this.state ={
			cards:[]
		}
	}

	componentDidMount(){ 
		this.requestCards()
	}

	requestCards = async () => {
		const result = await DrunknatorService.getListCartas()
		this.setState({cards:result})
	}

	renderCards = () =>{
		const cards = this.state.cards
		if(cards!= null && cards.length != 0){
			return cards.map((card,i) => (
				<CardDeck style={{ width: '18rem' }} key={i}>
					<Card bg="light" border="info">
					    <Card.Img variant="top" src={'http://'+card.imagem} />
					    <Card.Body>
					      <Card.Title>{card.titulo}</Card.Title>
					      <Card.Text>
					        {card.descricao}
					      </Card.Text>
					      <Container fluid="true" className="d-flex p-0 m-0 justify-content-between flex-row">
					      	<Button variant="primary">Editar</Button>
					      	<Button variant="danger">Apagar</Button>
					      </Container>
					    </Card.Body>
					</Card>
				</CardDeck>
			))
		}
	}

	render(){
		return(
			<Container>
				<Container className="d-flex flex-column">
					{this.renderCards()}
			  </Container>
		  </Container>
		)
	}
}