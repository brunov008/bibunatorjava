import React, { Component } from 'react'
import {Container,Button,Form,Col} from 'react-bootstrap'
import {Carta} from '../../models'
import {CustomModal} from '../customModal'

import {DrunknatorService} from '../../services';

export class Inserir extends Component{

	constructor(props){
		super(props)
		this.state={
			tipos:[],
			tituloValidation:false,
			descricaoValidation:false,
			tipoValidation:false,
			imagemValidation:false,
			titulo:null,
			descricao:null,
			tipo:null,
			imagem:null,
			isAdmin:false,
			modalShow:false
		}
	}

	componentDidMount(){
		this.requestTipos()
	}

	requestTipos = async () =>{
		const result = await DrunknatorService.listarTipos(this.props.tokenUsuario)
		this.setState({tipos:result.tipos, isAdmin:result.admin})
	}

	handleChange = async event =>{
		const { target } = event
		const value = target.value
		switch(target.id){
			case "tituloForm":
				if(value !== ""){
					await this.setState({titulo:value,tituloValidation:true})
				}else{
					await this.setState({tituloValidation:false})
				}
			break;

			case "descricaoForm":
				if(value !== "") {
					await this.setState({descricao:value,descricaoValidation:true})
				}else{
					await this.setState({descricaoValidation:false})
				}
			break;

			case "tipoForm":
				if(value != null){
					await this.setState({tipo:value,tipoValidation:true})
				}else{
					await this.setState({tipoValidation:false})
				}
			break;

			case "imagem":
				if(event.target.files.length !== 0){
					await this.setState({imagem:event.target.files[0],imagemValidation:true})
				}else{
					await this.setState({imagemValidation:false})
				}
			break;
			
			default:
			break;
		}
	}

	checkValidation = () =>{
		return (this.state.tituloValidation
			&& this.state.descricaoValidation
			&& this.state.tipoValidation
			&& this.state.imagemValidation)
	}

	handleSubmit = async event =>{
		event.preventDefault()

		const form = event.currentTarget
		if (form.checkValidity() === true) {
			//const carta = new Carta(this.state.tipo, this.state.titulo, this.state.descricao, this.state.imagem) nao esta funcionando
			// type text do input no react entra em um loop desnecessario
			const body = {
				cartaTipo: this.state.tipo,
				titulo: this.state.titulo,
				descricao:this.state.descricao,
				imagem:this.state.imagem
			}

			const result = await DrunknatorService.criarCarta(body, this.props.tokenUsuario)

			if(result.message){
				alert(result.message)
				this.props.onPerfilComponentChange("listar")
			}
		}
	}

	renderSelectItens = () =>{
		const tipos= this.state.tipos
		if(tipos != null && tipos.length != 0){
			return tipos.map( (value,i) => (
				<option key={i}>{value}</option>
			))
		}
	}



	renderButtonInserirTipo = () =>{
		if(this.state.isAdmin){
			return <Button variant="primary" onClick={() => this.setState({modalShow:true})}>
					  Adicionar tipo
					</Button>
		}
	}

	handleFormModal = async tipo =>{
		this.setState({modalShow:false})
		const result = await DrunknatorService.criarTipo(tipo,this.props.tokenUsuario)
		if(result.message != null && result.tipo != null){
			alert(result.message)
			this.requestTipos()
		}
	}

	render(){
		return(
			<Container fluid="true" className="d-flex pl-5">
				<CustomModal onFormComplete={this.handleFormModal} show={this.state.modalShow} onHide={() => this.setState({modalShow:false})}/>
				<Container className="d-flex flex-column pl-5 pr-5 pt-5">
					<Form noValidate 
					validated={this.checkValidation}
					onSubmit={this.handleSubmit}>
						<Form.Group>
							<Form.Label>Titulo</Form.Label>
							<Form.Control 
						    id="tituloForm" 
						    type="text" 
						    placeholder="Titulo"
						    isValid={this.state.tituloValidation === true}
						    isInvalid={this.state.tituloValidation ===false}
						    onChange={this.handleChange} 
						    required/>
						    <Form.Control.Feedback>Titulo ok!</Form.Control.Feedback>
						    <Form.Control.Feedback type="invalid">Titulo inválido</Form.Control.Feedback>
						</Form.Group>

						<Form.Group>
							<Form.Label>Descricao</Form.Label>
							<Form.Control 
						    id="descricaoForm" 
						    type="text" 
						    placeholder="Descricao"
						    isValid={this.state.descricaoValidation === true}
						    isInvalid={this.state.descricaoValidation ===false}
						    onChange={this.handleChange} 
						    required/>
						    <Form.Control.Feedback>Descricao ok!</Form.Control.Feedback>
						    <Form.Control.Feedback type="invalid">Descricao inválido</Form.Control.Feedback>
						</Form.Group>

						<Form.Group>
							<Form.Row>
								<Col>
									<Form.Control 
									id="tipoForm" 
									as="select"
									isValid={this.state.tipoValidation === true}
								    isInvalid={this.state.tipoValidation ===false}
									onChange={this.handleChange} 
									required>
										{this.renderSelectItens()}
									</Form.Control>
								</Col>
								<Col>
									{this.renderButtonInserirTipo()}
								</Col>
							</Form.Row>
						</Form.Group>

						<Form.Group className="mt-4">
							<Form.Control 
							type="file" 
							id="imagem"
							isValid={this.state.imagemValidation === true}
						    isInvalid={this.state.imagemValidation ===false} 
							onChange={this.handleChange} 
							required/>
						</Form.Group>

						<Button variant="primary" type="submit">
						    Inserir
						 </Button>
					</Form>
				</Container>
			</Container>
		)
	}
}