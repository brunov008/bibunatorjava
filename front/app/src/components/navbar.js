import React,{Component} from 'react';
import {Navbar,Nav,Container,NavDropdown} from 'react-bootstrap';
import { Redirect } from 'react-router-dom'

export class CustomNavbar extends Component{

	constructor(props){
		super(props)
		this.state = {
			redirect:false,
			components:['login','perfil'],
			render:null
		}
	}
	
	onBibunatorButtonCLick = () =>{
		this.props.onComponentChange("drunknator")
	}

	onApp2ButtonClick = () =>{
		this.props.onComponentChange("app2")
	}

	onApp3ButtonClick = () =>{
		this.props.onComponentChange("app3")
	}

	onHomeButtonClick = () =>{
		this.props.onComponentChange("home")
	}

	onLoginButtonClick = () =>{
		this.setState({
			redirect:true,
			render:this.state.components[0]
		})
	}

	onPerfilButtonClick = () =>{
		this.setState({
			redirect:true,
			render:this.state.components[1]
		})
	}

	handleComponentChange = () =>{
		if(this.state.redirect){
			switch(this.state.render){
				case this.state.components[0]:
					return <Redirect push to="/usuario/login"/>
				break

				case this.state.components[1]:
					return <Redirect push to="/perfil"/>
				break

				default:
				break
			}
		}
	}

	handleUserLogged = () =>{
		if(this.props.user != null){
			return <NavDropdown title={this.props.user.nome} id="basic-nav-dropdown">
			        <NavDropdown.Item onClick={this.onPerfilButtonClick}>Perfil</NavDropdown.Item>
			        <NavDropdown.Item href="#action/3.2">Logout</NavDropdown.Item>
			      </NavDropdown>
		}else{
			return <Nav.Link onClick={this.onLoginButtonClick}>Login</Nav.Link>
		}
	}

	render(){
		return(
			<Container fluid={true} className={this.props.className}>
				{this.handleComponentChange()}
				<Navbar expand={true} bg="dark" variant="dark">
				    <Navbar.Brand onClick={this.onHomeButtonClick}>Home</Navbar.Brand>
				    <Navbar.Collapse id="responsive-navbar-nav">
					    <Nav className="mr-auto">
					      <Nav.Link href="#">Novidades</Nav.Link>
					      <NavDropdown title="Jogos" id="collasible-nav-dropdown">
					        <NavDropdown.Item onClick={this.onBibunatorButtonCLick}>Bibunator</NavDropdown.Item>
					        <NavDropdown.Item onClick={this.onApp2ButtonClick}>App2</NavDropdown.Item>
					        <NavDropdown.Item onClick={this.onApp3ButtonClick}>App3</NavDropdown.Item>
					      </NavDropdown>
					    </Nav>
					    <Nav className="justify-content-end mr-5">
							{this.handleUserLogged()}
						</Nav>
					</Navbar.Collapse>
				</Navbar>
			</Container>
		)
	}
}