import axios from 'axios';
import Service from './mainService'

export class DrunknatorService extends Service{
  static getRandomCarta = async () => {
  	try{
	    const res = await axios.get(`http://localhost:3032/api/drunknator/carta`)
	    return res.data || []
  	}catch(e){
  		return []
  	}
  }

  static getListCartas = async () => {
    try{
      const res = await axios.get(`http://localhost:3032/api/drunknator/cartas`)
      return res.data.result || []
    }catch(e){
      return []
    }
  }

  static listarTipos = async (token) => {
    const headers = {
      'x-access-token': token
    }

  	try{
  		const res = await axios.get(`http://localhost:3032/api/drunknator/tipos`,{
        headers: headers
      })
      return res.data || []
  	}catch(e){
  		return []
  	}
  }

  static criarTipo = async (tipo, token) => {
    const headers = {
      'Content-Type': 'application/json',
      'x-access-token': token
    }

    const body = {
      tipo:tipo
    }

    try{
      const res = await axios.put('http://localhost:3032/api/drunknator/tipos/create',body,{
        headers:headers
      })
      return res.data
    }catch(e){
      return []
    }
  }

  static criarCarta = async ({cartaTipo,titulo,descricao,imagem}, token) => {
    const headers = {
      'Content-Type': 'multipart/form-data',
      'x-access-token': token
    }

    const bodyFormData = new FormData()
    bodyFormData.append('cartaTipo',cartaTipo)
    bodyFormData.append('titulo',titulo)
    bodyFormData.append('descricao',descricao)
    bodyFormData.append('image', imagem)

    try{
      const res = await axios.post(`http://localhost:3032/api/drunknator/cartas/create`,bodyFormData,{
        headers: headers
      })
      return res.data || []
    }catch(e){
      return []
    }
  }
}