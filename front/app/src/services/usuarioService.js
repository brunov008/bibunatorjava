import axios from 'axios';

export class UsuarioService{

	static cadastrar = async (user) => {
		try{
			const res = await axios.put(`http://localhost:3032/api/usuario/create`,{
				email:user.email,
				nome:user.nome,
				senha:user.senha
			})
			return res.data || null
		}catch(e){
			return []
		}
	}

  	static logar = async (user) => {
	  	try{
		    const res = await axios.post(`http://localhost:3032/api/usuario/authenticate`,{
			    email: user.emailUsuario,
			    senha: user.senhaUsuario
			})
		    return res.data || null
		}catch(e){
			return {message:"Ocorreu um erro ao se conectar"} 
		}
	}
}