package bruno.bibunator.resource;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import bruno.bibunator.model.Model;

public final class CustomResponse{
	
	public Response buildResponse(Model response) {
		System.out.println("BUILD RESPONSE");
		ResponseBuilder responseBuilder;
		
		if(response != null) {
			if(response.getStatus() != 200) {
				responseBuilder = Response.status(response.getStatus(), response.getMessage());
			}else {
				responseBuilder = Response.ok().entity(response);
			}
		}else {
			responseBuilder = Response.status(501);
		}
		
		return responseBuilder.build();
	}
}
