package bruno.bibunator.resource;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bruno.bibunator.ejb.CartasEJB;
import bruno.bibunator.ejb.TipoEJB;
import bruno.bibunator.model.CartaResponse;
import bruno.bibunator.model.TipoRequest;
import bruno.bibunator.model.TipoResponse;

@Path("drunknator")
@Named("drunknatorBean")
@Stateless
public class Drunknator {
	
	@Inject private CartasEJB cartasService;
	
	@Inject private TipoEJB tipoService;
	
	@PUT
	@Path("tipos/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createTipo(TipoRequest request) {
		TipoResponse response = tipoService.criarTipo(request);
		
		return new CustomResponse().buildResponse(response);
	}
	
	@GET
	@Path("carta")
	@Produces(MediaType.APPLICATION_JSON)
	public Response randomCard() {
		//CartaResponse response = cartasService.cartaRandomica();
				
		return new CustomResponse().buildResponse(null);
	}
	
	@GET
	@Path("cartas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CartaResponse> listAllCards() {
		return cartasService.findAll();
	}
}

