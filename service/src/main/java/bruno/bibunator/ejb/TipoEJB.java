package bruno.bibunator.ejb;

import javax.ejb.DependsOn;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import bruno.bibunator.TipoCartas;
import bruno.bibunator.model.TipoRequest;
import bruno.bibunator.model.TipoResponse;

@Named("tiposBean")
@DependsOn(value = { "drunknatorBean" })
@RequestScoped
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoEJB {
	
	@Inject
	private EntityManager manager;
	
	public TipoResponse criarTipo(TipoRequest request) {
		
		TipoCartas carta = new TipoCartas();
		
		this.manager.persist(carta);
		
		return null; //FIXME
	}
}
