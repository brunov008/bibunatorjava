package bruno.bibunator.ejb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.DependsOn;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

import bruno.bibunator.CartasDrunknator;
import bruno.bibunator.model.CartaResponse;

/**
* CartasEJB é uma classe do tipo EJB definido com escopo Request, isto é, o bean será iniciado e destruido apenas
* durante a requisicao. A anotacao TransactionManagement se refere ao tipo de gerenciamento da transacao no 
* banco de dados utilizando o JTA. Foi utilizado o tipo container para que o JTA gerencie a transacao.
* Este EJB tem funcao de acessar o banco de dados através do entity manager(objecto da api hibernate)
* que trata a persistencia dos dados no banco.
*
* @author  Bruno Oliveira
* @version 1.0
* @since   2020-01-29 
*/

@Named("cartasBean")
@DependsOn(value = { "drunknatorBean" })
@RequestScoped
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CartasEJB{
	
	@Inject
	private EntityManager manager;
	
	/**
	 * @return Lista de Cartas do banco
	 */
	public List<CartaResponse> findAll(){
		ArrayList<CartaResponse> result = new ArrayList<CartaResponse>();
		
		this.manager.createQuery("select a from CartasDrunkNator a", CartasDrunknator.class)
			.getResultList()
			.stream()
			.forEach(value -> {
				CartaResponse carta = new CartaResponse();
				carta.setTitulo(value.getTitulo());
				result.add(carta);
			});
		
		return result;
	}
	
	/**
	 * @return Objeto CartaResponse
	 * @exception SqlException ao tentar acessar o banco
	 */
	public CartaResponse cartaRandomica(){
		System.out.println("CARTASEJB");
		CartaResponse carta = new CartaResponse();
		
		try {
			DataSource ds = InitialContext.doLookup("java:/bibunatorDS");
			Connection cnn = ds.getConnection();
			PreparedStatement ps = cnn.prepareStatement("select * from bibunator.CartasDrunkNator order by rand() limit 1");
			ResultSet result = ps.executeQuery();
			
			if(result.next()) {
				carta.setId(result.getInt("idCarta"));
				carta.setTitulo(result.getString("titulo"));
				carta.setImagemPath(result.getString("imagem"));
				carta.setTipoCartas_idTipo(result.getInt("TipoCartas_idTipo"));
				carta.setDescricao(result.getString("descricao"));
				carta.setMessage("Ok");
				carta.setStatus(200);
			}
			
			result.close();
			ps.close();
			cnn.close();
			
			return carta;
		}catch(Exception e) {
			System.out.println("EXCESSAO DB");
			carta.setMessage("Ocorreu um erro na conexao com o banco "+e.getCause());
			carta.setStatus(500);
			return carta;
		}
	}
}
