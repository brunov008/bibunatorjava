package bruno.bibunator.model;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

public class TipoRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonbProperty("tipo")
	private String tipo;
	
	public TipoRequest() {
		
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
