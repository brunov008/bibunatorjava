package bruno.bibunator.model;

import javax.json.bind.annotation.JsonbProperty;

public final class CartaResponse extends Model{

	private static final long serialVersionUID = 1L;
	
	@JsonbProperty("id")
	private Integer id;
	
	@JsonbProperty("titulo")
	private String titulo;
	
	@JsonbProperty("descricao")
	private String descricao;
	
	@JsonbProperty("imagemPath")
	private String imagem;
	
	//@JsonbProperty("TipoCartas_idTipo")
	private Integer TipoCartas_idTipo;
	
	public CartaResponse() {
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getTitulo() {
		return this.titulo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagemPath() {
		return imagem;
	}

	public void setImagemPath(String imagem) {
		this.imagem = imagem;
	}

	public Integer getTipoCartas_idTipo() {
		return TipoCartas_idTipo;
	}

	public void setTipoCartas_idTipo(Integer tipoCartas_idTipo) {
		TipoCartas_idTipo = tipoCartas_idTipo;
	}
	
	

}
