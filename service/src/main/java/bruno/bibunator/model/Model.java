package bruno.bibunator.model;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

public abstract class Model implements Serializable{
	public static final long serialVersionUID = 1L;
	
	@JsonbProperty("message")
	private String message;
	
	private Integer status;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
